![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---
List of companies (and their CEOs) doing bad stuff on the Internet:

1. 2019, Priceline: false advertising (https://news.ycombinator.com/item?id=20285693) - CEO: Brett Keller, https://www.linkedin.com/in/brett-keller-9521871/
2. 2019, GrubHub: cybersquatting; fake websites to rack commissions (https://news.ycombinator.com/item?id=20321260) - CEO: Matt Maloney, https://about.grubhub.com/about-us/executive-team/default.aspx
3. 2019, Eat24: cybersquatting; fake websites to rack commissions (https://news.ycombinator.com/item?id=20321838) - CEO: Nadav Sharon, https://www.linkedin.com/in/nadav-sharon-383a6119/
4. 2019, Slice (Pizza Delivery app): cybersquatting; fake websites to rack commissions (https://news.ycombinator.com/item?id=20321974) - CEO: Ilir Sela, https://www.linkedin.com/in/ilirsela/
5. 2019, Zoom: Zoom is reinstalling itself after a minute of being uninstalled (https://news.ycombinator.com/item?id=20390755) - CEO: Eric Yuan, https://www.linkedin.com/in/ericsyuan2011/
6. 2019, Pluralsight: auto-renewal dark pattern (https://news.ycombinator.com/item?id=20464486) - CEO: Aaron Skonnard, https://www.linkedin.com/in/skonnard/

---

